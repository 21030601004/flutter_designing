import 'package:flutter/material.dart';
import 'package:footer/footer.dart';
class PageFour extends StatelessWidget {
  const PageFour({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title:
      Row(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              child: CircleAvatar(
                foregroundImage: NetworkImage('https://media.istockphoto.com/id/1489498508/photo/scary-figure-with-hooded-cloak-in-the-dark.webp?b=1&s=612x612&w=0&k=20&c=ScjRj0Z043cny_znUhUxTU8Qtnnxv48C-uYo32rj5a4='),
                backgroundColor: Colors.black,
                ),
            ),
          ),
          Column(
            children: [
              Text('Aayu',style: TextStyle(fontSize: 20,color: Colors.black),),
            ],
          ),
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(Icons.arrow_forward_ios_outlined,color: Colors.black,size: 15,),
              ),
            ],
          ),
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 200,right: 8,top: 8,bottom: 8),
                child: Icon(Icons.notification_add_outlined,color: Colors.black,size: 25,),
              ),
            ],
          ),
        ],
      ),
        backgroundColor: Colors.white,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(25.0),
            child: TextFormField(
                decoration: InputDecoration(
                border: OutlineInputBorder(),
                icon:Icon(Icons.search),
                hintText: 'Search',
            ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Row(
              children: [
                Text('Your totle balance in USD',style: TextStyle(fontSize: 18,color: Colors.black),),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Row(
              children: [
                Text('38,451.37 dollar',style: TextStyle(fontSize: 28,color: Colors.black,fontWeight: FontWeight.w700),),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Row(
              children: [
                Text('+ 562.123 doller in total',style: TextStyle(fontSize: 18,color: Colors.green,),),
              ],
            ),
          ),
          Stack(
            children: [
              Image.network('https://cdn.pixabay.com/photo/2018/07/29/10/13/crypto-3569788_640.jpg',height: 180,width:1000),
              Padding(
                padding: const EdgeInsets.only(left: 60,top: 70),
                child: Text('Crypto Currency Exchange',style: TextStyle(fontSize: 20,fontWeight: FontWeight.w500,color: Colors.white),),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 60,top: 100),
                child: Text('Trusted by You,High fees LOL,\nSlowest trades,INR,EUR and GBP',style: TextStyle(fontSize: 15,color: Colors.white),),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 330,top: 20),
                child: Icon(Icons.close,color: Colors.white,size: 20,),
              ),
            ],
          ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text('Coins',style: TextStyle(fontSize: 23,fontWeight: FontWeight.w600),),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 260,right: 15,top: 15,bottom: 15),
                      child: Icon(Icons.sort_rounded,size: 30,color: Colors.black,),
                    ),
                  ],
                ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Row(
                  children: [
                    Column(
                      children: [
                        Row(
                          children: [
                            Text('chitcoin',style: TextStyle(fontSize: 15,color: Colors.grey),),
                            Text(' +4.98%',style: TextStyle(fontSize: 15,color: Colors.red),),
                          ],
                        ),
                        Row(
                          children: [
                            Text('0.07654 BTC',style: TextStyle(fontSize: 20,color: Colors.black,),textAlign: TextAlign.left,),
                            Text(' ~3.114.98',style: TextStyle(fontSize: 15,color: Colors.black),),
                          ],
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 140,),
                      child: Container(
                        child: CircleAvatar(
                          foregroundImage: NetworkImage('https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Bitcoin.svg/800px-Bitcoin.svg.png'),
                          backgroundColor: Colors.orange,
                          minRadius: 25,
                        ),
                      ),
                    ),
                  ],
                ),


              ),

            ],
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Row(
                  children: [
                    Column(
                      children: [
                        Row(
                          children: [
                            Text('Ethereum',style: TextStyle(fontSize: 15,color: Colors.grey),),
                            Text(' +1.24%',style: TextStyle(fontSize: 15,color: Colors.green),),
                          ],
                        ),
                        Row(
                          children: [
                            Text('0.34242 ETH',style: TextStyle(fontSize: 20,color: Colors.black,),textAlign: TextAlign.left,),
                            Text(' ~511.12',style: TextStyle(fontSize: 15,color: Colors.black),),
                          ],
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 160,),
                      child: Container(
                        child: CircleAvatar(
                          foregroundImage: NetworkImage('https://cdn.icon-icons.com/icons2/2108/PNG/512/ethereum_icon_130942.png'),
                          backgroundColor: Colors.grey,
                          minRadius: 25,
                        ),
                      ),
                    ),
                  ],
                ),

              ),

            ],
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Row(
                  children: [
                    Column(
                      children: [
                        Row(
                          children: [
                            Text('Chainlink',style: TextStyle(fontSize: 15,color: Colors.grey),),
                            Text(' +9.134%',style: TextStyle(fontSize: 15,color: Colors.green),),
                          ],
                        ),
                        Row(
                          children: [
                            Text('4.342 LINK',style: TextStyle(fontSize: 20,color: Colors.black,),textAlign: TextAlign.left,),
                            Text(' ~2,234.62',style: TextStyle(fontSize: 15,color: Colors.black),),
                          ],
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 142,),
                      child: Container(
                        child: CircleAvatar(
                          foregroundImage: NetworkImage('https://s2.coinmarketcap.com/static/img/coins/200x200/1975.png'),
                          backgroundColor: Colors.grey,
                          minRadius: 25,
                        ),
                      ),
                    ),
                  ],
                ),

              ),

            ],
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Row(
                  children: [
                    Column(
                      children: [
                        Row(
                          children: [
                            Text('Tether',style: TextStyle(fontSize: 15,color: Colors.grey),),
                            Text(' +11.44%',style: TextStyle(fontSize: 15,color: Colors.green),),
                          ],
                        ),
                        Row(
                          children: [
                            Text('2.3535 USDT',style: TextStyle(fontSize: 20,color: Colors.black,),textAlign: TextAlign.left,),
                            Text(' ~23.55',style: TextStyle(fontSize: 15,color: Colors.black),),
                          ],
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 157,),
                      child: Container(
                        child: CircleAvatar(
                          foregroundImage: NetworkImage('https://iconape.com/wp-content/png_logo_vector/tether-usdt-logo.png'),
                          backgroundColor: Colors.grey,
                          minRadius: 25,
                        ),
                      ),
                    ),
                  ],
                ),

              ),

            ],
          ),
          Footer(
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 25,right: 25,top: 8,bottom: 8),
                      child: Column(
                        children: [
                          Icon(Icons.home,size: 25,color:Colors.green ,),
                          Text('HOME'),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 25,right: 25,top: 8,bottom: 8),
                      child: Column(
                        children: [
                          Icon(Icons.bar_chart,size: 25,color:Colors.grey ,),
                          Text('MARKET',style: TextStyle(color: Colors.grey),),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 25,right: 25,top: 8,bottom: 8),
                      child: Column(
                        children: [
                          Icon(Icons.compare_arrows,size: 25,color:Colors.grey ),
                          Text('ACTIONS',style: TextStyle(color: Colors.grey),),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 25,right: 25,top: 8,bottom: 8),
                      child: Column(
                        children: [
                          Icon(Icons.chat_sharp,size: 25,color:Colors.grey ),
                          Text('CHAT',style: TextStyle(color: Colors.grey),),
                        ],
                      ),
                    ),
                    ],
                ),
              ),
              backgroundColor : Colors.white,  // defines the background Colors of the Footer with default Colors.grey.shade200
              padding: EdgeInsets.all(5.0),// Takes EdgeInsetsGeometry with default being EdgeInsets.all(5.0)
              alignment: Alignment.bottomCenter //this is of type Aligment with default being Alignment.bottomCenter
          )
              ],
            ),
    );

  }
}
